# mpvue-prime

> 质数工具箱

水水的旧代码合集: 各种陈旧的代码，主要是学习探索时的记录；：

[https://gitee.com/wdssmq/StaleCode](https://gitee.com/wdssmq/StaleCode "水水的旧代码合集: 各种陈旧的代码，主要是学习探索时的记录；")

## Build Setup

``` bash
# cnpm
npm install -g cnpm --registry=https://registry.npmmirror.com

# 初始化项目
cd mpvue-prime

# 安装依赖
cnpm i

# 开发时构建
npm run dev

# 打包构建
npm build

# 指定平台的开发时构建(微信、百度、头条、支付宝)
npm dev:wx
npm dev:swan
npm dev:tt
npm dev:my

# 指定平台的打包构建
npm build:wx
npm build:swan
npm build:tt
npm build:my

# 生成 bundle 分析报告
npm run build --report
```
