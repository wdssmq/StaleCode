# 将网址保存至 *.url 文件

## 直接上演示吧

示例数据，在脚本目录内创建`input.txt`并写入；

<!--more-->

```conf
https://www.bilibili.com/video/BV1qe4y1d7ZM
https://www.bilibili.com/video/BV1mv4y1u79j
https://www.bilibili.com/video/BV1iv4y1K7iy
```

执行`main.py`;

执行结果；

```bash
/output
# BV1iv4y1K7iy.url
# BV1mv4y1u79j.url
# BV1qe4y1d7ZM.url
```

## 下载

py-url-2-url-file · 沉冰浮水/水水的旧代码合集 - 码云 - 开源中国：

[https://gitee.com/wdssmq/StaleCode/tree/master/py-url-2-url-file](https://gitee.com/wdssmq/StaleCode/tree/master/py-url-2-url-file "py-url-2-url-file · 沉冰浮水/水水的旧代码合集 - 码云 - 开源中国")


## 投喂支持

二维码：[https://gitee.com/wdssmq/StaleCode/tree/master#二维码](https://gitee.com/wdssmq/StaleCode/tree/master#二维码 "master#二维码")

爱发电：[https://afdian.net/a/wdssmq](https://afdian.net/a/wdssmq "沉冰浮水正在创作和 z-blog 相关或无关的各种有用或没用的代码 | 爱发电")

更多「小代码」：[https://cn.bing.com/search?q=小代码+沉冰浮水](https://cn.bing.com/search?q=%E5%B0%8F%E4%BB%A3%E7%A0%81+%E6%B2%89%E5%86%B0%E6%B5%AE%E6%B0%B4 "小代码 沉冰浮水 - 搜索")
