
```bash
# https://github.com/wdssmq/userscript/blob/main/dist-lib/lib-paiad.js

# 通过 jsdelivr 获取文件
wget https://cdn.jsdelivr.net/gh/wdssmq/userscript@main/dist-lib/lib-paiad.js \
    -O lib-paiad.js

# 通过 raw.githubusercontent.com 获取文件
wget https://raw.githubusercontent.com/wdssmq/userscript/main/dist-lib/lib-paiad.js \
    -O lib-paiad.js

# 开发模式
wget http://localhost:10001/dist/lib-paiad.js -O lib-paiad.js


```
