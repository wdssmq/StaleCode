# 水水的旧代码合集

#### 介绍
各种旧代码，虽然有些并没有很旧；

## 二维码↓
## 投喂支持

<table>
  <tr>
    <td>
      <img
        src="https://fastly.jsdelivr.net/gh/wdssmq/wdssmq@main/doc/qr-ali.png"
        alt="qr-ali"
        title="qr-ali"
      />
    </td>
    <td>
      <img
        src="https://fastly.jsdelivr.net/gh/wdssmq/wdssmq@main/doc/qr-wx.png"
        alt="qr-wx"
        title="qr-wx"
      />
    </td>
    <td>
      <img
        src="https://fastly.jsdelivr.net/gh/wdssmq/wdssmq@main/doc/qr-qq.png"
        alt="qr-qq"
        title="qr-qq"
      />
    </td>
  </tr>
  <tr>
    <td align="center" colspan="3">
      <a
        target="_blank"
        href="https://afdian.net/@wdssmq"
        title="沉冰浮水正在创作和 z-blog 相关或无关的各种有用或没用的代码 | 爱发电"
        ><img
          src="https://fastly.jsdelivr.net/gh/wdssmq/wdssmq@main/doc/afdian.png"
          alt="爱发电"
      /></a>
    </td>
  </tr>
</table>
