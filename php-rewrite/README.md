## php-rewrite

[「水坑」略深入的讲解伪静态相关的知识](https://www.wdssmq.com/post/20190704012.html "「水坑」略深入的讲解伪静态相关的知识")

### 效果图（纯动态模式）

![001-home](doc/001-home.png)

![002-post](doc/002-post.png)

![003-err](doc/003-err.png)

![004-err](doc/004-err.png)
